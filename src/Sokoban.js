import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';

import './Sokoban.css';
import PageContainer from './modules/PageContainer';
import { DOWN, TOP, LEFT, RIGHT, INIT } from './modules/constants';

class App extends Component {
    render() {
        const { state } = this.props;
        const count = state.boardState !== INIT
            ?  <div className="counter">{ state.count } / { Math.min(state.boxCount, state.goalCount) }</div>
            : null;
        return (
            <div className="page">
                <div className="page-header">
                    <header>
                        <h1>Sokoban<span>Game</span></h1>
                        { count }
                    </header>
                </div>
                <PageContainer />
                <footer>
                    <a href="https://gitlab.com/lutretyakova/react-sokoban-with-levels">
                        https://gitlab.com/lutretyakova/react-sokoban-with-levels
                    </a>
                </footer>
            </div>
        );
    }


    componentDidMount() {
        document.addEventListener("keydown", this.handleKeyDown.bind(this));
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.handleKeyDown.bind(this));
    }

    handleKeyDown(event) {
        if ([DOWN, TOP, LEFT, RIGHT].includes(event.keyCode)) {
            const { changeCourse } = this.props;
            changeCourse(event.keyCode);
        }
    }
}

export default connect(state => ({ state }), actions)(App);
