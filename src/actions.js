export const OPEN_LEVEL = 'openLevel';

export const CHANGE_COURSE = 'changeCourse';

export const RESTART_GAME = 'restartGame';

export const openLevel = (gameLevel) => ({
    type: OPEN_LEVEL,
    gameLevel
});

export const changeCourse = (keyCode) => ({
    type: CHANGE_COURSE,
    keyCode
});

export const restart = () => ({
    type: RESTART_GAME
})
