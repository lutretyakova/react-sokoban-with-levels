import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions.js';

import ListPanel from './ListPanel';
import GameBoard from './GameBoard';
import { INIT } from './constants';

class PageContainer extends Component {
    render() {
        const { state } = this.props;
        const gameBoard = state.boardState !== INIT 
            ? <GameBoard key={state.key} />
            : null;
        return(
            <div className="container">
                { gameBoard }
                <ListPanel />
            </div>
        )
    }
}

export default connect((state) => ({ state }), actions)(PageContainer);