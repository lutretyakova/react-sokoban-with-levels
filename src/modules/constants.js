export const INIT = 'init';

export const LOADED = 'loaded';

export const RESTART = 'restart';

export const SIZE_BLOCK = 42;

export const EMPTY = 0;

export const HUMAN = 1;

export const WALL = 2;

export const BOX = 3;

export const GOAL = 4;

// array key codes
export const DOWN = 40;

export const TOP = 38;

export const RIGHT = 39;

export const LEFT = 37;