import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../actions';

class WinWindow extends Component {
    constructor(props) {
        super(props);
        const container = document.getElementsByClassName("container")[0];
        this.height = container.clientHeight;
    }

    render() {
        const styleDiv = { 
            height: `${this.height}px`,
        }
        return (
            <dialog className="modal-win" style={styleDiv} open>
                <h1>Поздравляем с победой!!!</h1>
                <div className="btn" onClick={() => this.handleClick()}>Хочу еще раз!</div>
            </dialog>
        )
    }

    handleClick() {
        const { restart } = this.props;
        restart();
    }
}

export default connect((state) => ({ state }), actions)(WinWindow)