import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import { SIZE_BLOCK } from './constants';

class AnimationCount extends Component {
    constructor(props) {
        super(props);

        this.width = document.getElementsByClassName("container")[0].clientWidth - 
            document.getElementsByClassName("list-pnl")[0].clientWidth ;
        this.height = SIZE_BLOCK * this.props.state.height;
    }

    render() {
        const count = this.props.state.count;
        const styleDiv = { 
            height: `${this.height}px`,
            width: `${this.width}px`,
            position: 'absolute',
            left: 0
        }
        return (
            <div className="modal animation" style={styleDiv}>
                {count}
            </div>
        )
    }
}

export default connect((state) => ({state}), actions)(AnimationCount);