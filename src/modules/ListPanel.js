import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';

import {
    Stitch, RemoteMongoClient, AnonymousCredential
} from "mongodb-stitch-browser-sdk";

import { INIT, RESTART } from './constants';

const client = Stitch.initializeDefaultAppClient('sokoban-ymorc');
const db = client.getServiceClient(RemoteMongoClient.factory, 'mongodb-atlas').db('sokoban');

class ListPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            active: -1,
        }
    }

    render() {
        const { state } = this.props;
        const levels = this.state.loaded
            ? Object.keys(this.state.levels).map((inxKey) => {
                let date = this.state.levels[inxKey].date !== undefined
                    ? new Date(this.state.levels[inxKey].date).toLocaleString('ru-ru')
                    : "???";
                const active = this.state.active === inxKey
                    ? "active"
                    : "";
                return <div className={`btn ${active}`} key={inxKey} id={inxKey} onClick={(e) => this.handleClick(e)}>
                    Уровень : {date}
                </div>
            })
            : null;

        const pnlClass = state.boardState === INIT
            ? "list-pnl"
            : "list-pnl right"
        return (
            <div className={pnlClass} >
                <div className="level-list">
                    {levels}
                </div>
            </div>
        )
    }

    componentDidMount() {
        client.auth.loginWithCredential(new AnonymousCredential())
            .then(user =>
                db.collection('level').find({isHumanSet: true}).asArray()
            ).then(levels => {
                this.saveLevels(levels);
            }).catch(err => {
                console.log(err)
            });
    }

    saveLevels(levels) {
        let records = {};
        for (let inx = 0; inx < levels.length; inx++) {
            records[inx] = levels[inx]
        }
        this.setState({
            levels: records,
            loaded: true
        })
    }

    handleClick(event) {
        const { openLevel } = this.props;
        openLevel(this.state.levels[event.target.id]);
        this.setState({
            active: event.target.id
        })
    }

    componentDidUpdate() {
        if (this.props.state.boardState === RESTART) {
            const { openLevel } = this.props;
            openLevel(this.state.levels[this.state.active]);
        }
    }

}

export default connect((state) => ({ state }), actions)(ListPanel)