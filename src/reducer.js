import { INIT, LOADED, RESTART } from "./modules/constants";
import { OPEN_LEVEL, CHANGE_COURSE, RESTART_GAME } from "./actions";

import { 
    HUMAN, EMPTY, BOX, GOAL, WALL,
    DOWN, LEFT, TOP, RIGHT
} from './modules/constants';

export default (state = init(), actions) => {
    switch (actions.type) {
        case OPEN_LEVEL:
            return openLevel(state, actions.gameLevel);
        case CHANGE_COURSE:
            return changeCourse(state, actions.keyCode);
        case RESTART_GAME:
            return restartGame(state);
        default:
            return state
    }
}

const init = () => {
    return {
        key: 1,
        boardState: INIT
    }
}


const openLevel = (state, gameLevel) => {
    const width = parseInt(gameLevel.width, 10);
    const height = parseInt(gameLevel.height, 10);

    const humanCoord = getCoords(gameLevel.board, HUMAN, width);
    const humanReg = new RegExp(HUMAN, "g");
    let mapLine = gameLevel.board
        .replace(/\s/g, '')
        .replace(humanReg, EMPTY);

    const boxReg = new RegExp(BOX, "g");
    const boxCount = gameLevel.board.match(boxReg)
        ? gameLevel.board.match(boxReg).length
        : 0;
    let boxes = [];
    for (let i = 0; i < boxCount; i++) {
        boxes.push(getCoords(mapLine, BOX, width));
        mapLine = mapLine.replace(BOX, EMPTY);
    }

    const goalReg = new RegExp(GOAL, "g");
    const goalCount = gameLevel.board.match(goalReg)
        ? gameLevel.board.match(goalReg).length
        : 0;
    let goals = [];
    for (let i = 0; i < goalCount; i++) {
        goals.push(getCoords(mapLine, GOAL, width));
        mapLine = mapLine.replace(GOAL, EMPTY);
    }

    mapLine = mapLine
        .split(',')
        .map(elem => parseInt(elem, 10));

    let board = (new Array(height)).fill(null)
        .map((_, inx) => (
            mapLine.slice(inx * width, (inx + 1) * width)
        ));

    return {
        ...state,
        board,
        width,
        height,
        boxCount,
        goalCount,
        humanCoord,
        boxes,
        goals,
        course: DOWN,
        count: 0,
        boardState: LOADED,
        key: state.key + 1
    }
}

const getCoords = (map, obj, width) => {
    const reg = new RegExp(obj, "g");
    const objPos = map.replace(/\s|,/g, "")
        .search(reg);
    const objCoord = [
        Math.floor(objPos / width),
        objPos % width
    ]

    return objCoord
}


const changeCourse = (state, keyCode) => {
    let course = keyCode;

    let [dy, dx] = [0, 0];
    switch (course) {
        case TOP:
            dy = -1;
            break;
        case RIGHT:
            dx = 1;
            break;
        case LEFT:
            dx = -1;
            break;
        case DOWN:
            dy = 1;
            break;
        default:
            return state
    }

    const newHumanCoord = [
        state.humanCoord[0] + dy,
        state.humanCoord[1] + dx
    ]

    let boxes = [];
    if (isInsideBoard(newHumanCoord, state.height, state.width) &&
        WALL !== state.board[newHumanCoord[0]][newHumanCoord[1]]) {
        if (!isCoordEquals(state.boxes, newHumanCoord)) {
            return {
                ...state,
                course,
                humanCoord: newHumanCoord,
                isShowAnimation: false
            }
        } else {
            const newBoxCoord = [
                newHumanCoord[0] + dy,
                newHumanCoord[1] + dx
            ];

            if (isInsideBoard(newBoxCoord, state.height, state.width) &&
                WALL !== state.board[newBoxCoord[0]][newBoxCoord[1]] &&
                !isCoordEquals(state.boxes, newBoxCoord)) {
                boxes = state.boxes.filter((item) => (
                    !(item[0] === newHumanCoord[0] && item[1] === newHumanCoord[1]))
                )
                boxes.push(newBoxCoord);

                const count = state.goals.filter(
                    (item) => (isCoordEquals(boxes, item))
                ).length;

                const isShowAnimation = count > state.count && count !== state.boxCount ? true : false;
                return {
                    ...state,
                    humanCoord: newHumanCoord,
                    course,
                    boxes,
                    count,
                    isShowAnimation
                }
            }
        }
    }
    return {
        ...state,
        course,
        isShowAnimation: false
    }
}


const isCoordEquals = (coordsArr, coord) => {
    return coordsArr
        .filter((item) => (item[0] === coord[0] && item[1] === coord[1]))
        .length > 0
}


const isInsideBoard = (coord, height, width) => {
    return coord[0] >= 0 && coord[0] < height &&
        coord[1] >= 0 && coord[1] < width
}


const restartGame = (state) => {
    return {
        ...state,
        boardState: RESTART
    }
}